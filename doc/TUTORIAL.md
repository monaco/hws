
  HELLO TWO WORLDS TUTORIAL
==================================================

  
  Copyright (c) 2018 Monacco F. J. <monaco@usp.br>
  
  For Licensing information please refer to the companion file COPYING
  distributed with this software package or alternatively accessible at
  the project repository at www.gitlab.com/monaco/h2
  
  Obtaining the project
--------------------------------------------------

  On a GNU/Linux or POSIX compliant platform, follow the directions bellow.

  a) If you are getting the project from the development VCS repository

     Make sure you have both Git and GNU Autotools installed

     $ git clone git@gitlab.com:monaco/h2w.git
     $ cd h2w
     $ ./autogen.sh

  of, alternatively,


  b) if you are getting the project from the distribution repository

     Download h2w tarball, say version x.y.z, and uncompress it with

     $ tar zxvf h2w-<x.y.z>.tar.gz
     $ cd h2w


  You will need to build and install the examples in order to try them out.
  In this tutorial I assume the project's source directory is

    $HOME/h2w

  (e.g, if $HOME is /home/snow, than you'll have /home/snow/h2w)

  and that you are going to install binaries (executable) under

    /tmp/foo

  If you want to experiment with the examples using a virtualized Windows
  installation, it may be a good idea to setup a shared folder accessible
  by both the host and guest OSes. For instance, if you share /tmp/foo,
  and this directory is visible as drive E: under Windows, than your
  examples will be visible under E:\bin.

  If your are building and installing the examples in different paths,
  please adjust the examples to match your choices.

  Notes on POSIX and Windows API differences
--------------------------------------------------

  
   * Error handling

    POSIX system functions which may fail, as it's the case of those who issue
    syscalls, explicity specify the value they reuturn on failure and define a
    unique error code reported through the thread-safe global variable errno.
    This is quite convenient to detect faults and take appropriate actions.
    Windows API, on the other hand, seems to be a bit more lax in this aspect,
    (perhaps confidently assuming failure is unlikely). 


    As annotated on Windows API manual []:    

      "_When an error occurs, most system functions return an error code,
      usually 0, NULL, or –1. Many system functions also set an additional error
      code called the last-error code [...]_ 
      
      _The error codes returned by a function are not part of the Windows 
       API specification and can vary by operating system or device driver. For 
       this reason, we cannot provide the complete list of error codes that can 
       be returned by each function. There are also many functions whose 
       documentation does not include even a partial list of error codes that
       can be returned [...]_

       _Note that some functions call SetLastError or SetLastErrorEx with 0 
       when they succeed, wiping out the error code set by the most recently 
       failed function, while others do not._"



  Notes on compiler name systems
----------------------------------------------------------------------

  GNU name scheme is a triplet in the form

  cpu-vendor-(os-)abi

  For instance

  * x86_64-w64-mingw32 = x86_64 architecture (=AMD64),
    		       	 w64 (=mingw-w64 as "vendor"),
			 mingw32 (=win32 API as seen by GCC)

  * i686-pc-msys = 32-bit (pc=generic name) msys binary

  * i686-unknown-linux-gnu = 32-bit GNU/linux

  * arm-none-linux-gnueabi = ARM architecture, no vendor, linux OS, and the gnueabi ABI.

  Caveat: Debian-based systems has a slighly modified GNU-triple scheme. See

  * doc/MultiarchSpec-Ubuntu-Wiki.pdf
  * doc/Multiarch_Tuples-Debian-Wiki.pdf

  Compiling native binaries for GNU/Linux on the build platform
--------------------------------------------------

  On your GNU/Linux station, from within $HOME/h2w, 
  

  $ ./configure --prefix=/tmp/foo
  $ make
  $ make install

  You should find your examples installed under /tmp/foo/bin
  The example bar.c shall be compiled into an executable file named bar.
  You may run it by issuing

  $ /tmp/foo/bin/bar

  or, alternative

  $ cd /tmp/bin
  $ ./bar

  (Cross)Compiling native binaries for GNU/Linux 64-bit
--------------------------------------------------

  On your GNU/Linux station, from within $HOME/h2w, 
  

  $ ./configure --prefix=/tmp/foo --host=x86_64-gcc-linux-gnu
  $ make
  $ make install

  You should find your examples installed under /tmp/foo/bin
  The example bar.c shall be compiled into an executable file named bar.
  You may run it by issuing

  $ /tmp/foo/bin/bar

  or, alternative

  $ cd /tmp/bin
  $ ./bar

  If your build manchine is not 64-bit, you shall see a message telling
  that the software was cross-compiled.

  (Cross)Compiling native binaries for GNU/Linux 32-bit
--------------------------------------------------

  On your GNU/Linux station, from within $HOME/h2w, 
  

  $ ./configure --prefix=/tmp/foo --host=i686-gcc-linux-gnu
  $ make
  $ make install

  You should find your examples installed under /tmp/foo/bin
  The example bar.c shall be compiled into an executable file named bar.
  You may run it by issuing

  $ /tmp/foo/bin/bar

  or, alternative

  $ cd /tmp/bin
  $ ./bar

  If your build manchine is not 32-bit, you shall see a message telling
  that the software was cross-compiled.

Cross compiling native binaries for MS Windows 64-bit
--------------------------------------------------

  On your GNU/Linux station, from within $HOME/h2w

  $ ./configure --prefix=/tmp/foo --host=x86_64-w64-mingw32
  $ make
  $ make install

  You should find your examples installed under /tmp/foo/bin.
  The example bar.c shall be complied into an executable file named bar.exe.
  You may copy it to a directory in your Windows machine, say E:\bin.
  Now, on your Windows machine, open a Command Prompt application (cmd) and

  C:> E:\
  E:> cd \bin
  E:\bin> bar.exe


  Cross compiling native binaries for MS Windows 32-bit
--------------------------------------------------

  On your GNU/Linux station, from within $HOME/h2w

  $ ./configure --prefix=/tmp/foo --host=i686-w64-mingw32
  $ make
  $ make install

  You should find your examples installed under /tmp/foo/bin.
  The example bar.c shall be complied into an executable file named bar.exe.
  You may copy it to a directory in your Windows machine, say E:\bin.
  Now, on your Windows machine, open a Command Prompt application (cmd) and

  C:> E:\
  E:> cd \bin
  E:\bin> bar.exe

  Compiling native binaries for MS Windows
--------------------------------------------------

  On your MS Windows station

  Run MSYS2.
  MSYS2 automatically mounts Windows driver C: under /c.
  Therefore, if h2w lays on E:\hwc, it will be accessible from within MSYS2
  as the path /e/h2w.
  In order to build the examples
  
  $ cd /e/h2w
  $ ./configure --prefix=/e
  $ make
  $ make install

  The example bar.c shall be compiled and installed as E:\bin\bar.exe

  NOTICE that the binary file generated by the MinGW crosscompilation and native compilation
  differ. Use objdump to check it out.

  Compiling native binaries for Gygwin (POSIX over Win32)
--------------------------------------------------

  On your MS Windows station

  Run Cygwin console.

  Cygwin automatically mounts Windows driver C: under /cygdrive/c.
  Therefore, if h2w lays on E:\hwc, it will be accessible from within MSYS2
  as the path /cygdrive/e/h2w.
  In order to build the examples
  
  $ cd /e/h2w
  $ ./configure --prefix=/e
  $ make
  $ make install

  The example bar.c shall be compiled and installed as E:\bin\bar.exe

  If you take a look at the output of configure script (or read config.log)
  you will see that both build and host autoconf variables are
  x86_64-unknown-cygwin  (if your platform is 64 bit, but it may be
  i686-unknown-cygwin if it'a a 32-bit platform).

  Binaries compiled with Cygwin's gcc will differ from both native
  and cross-compiled binaries generated by MinGW.

  In order to run your program from Cygwin terminal, just invoke it as usual

  $ /cygdriver/e/bin/bar.exe

  Notice that the binary file generated by the Cygwin will be dynamically
  linked agains cygwin1.dll, which provides the POSIX layer.  Therefore,
  if you just try to run bar.exe from Windows' Command Prompt, it is
  possible that the execution fails if cygwin1.dll is not installed
  in usual path where the OS looks for shared libraries. Two workarounds
  you may resort to are either adding cygwin1.dll location to your (or
  system's) PATH environment variable or copy cygwin1.dll to the
  same directory than bar.exe.

  In our example, we rely only on cygwin1.dll, but more sophisticated
  examples may need other DLLs provided by Cygwin.  In order to know
  which Cygwin DLLs your binary uses, on Cygwin console, run

  $ objdump.exe -x bar.exe | grep "\.dll"

  you shall obtain an output like

        DLL Name: cygwin1.dll
        DLL Name: KERNEL32.dll

  This informs that the executable bar.exe will need to access two
  DLLs. KERNEL32.dll is the Windows runtime, that should be natively available
  on any Windows OS. The other DLL, cygwin1.dll is the one you must ship
  with your executable.


  Therefore, if you want to distribute your binaries and not require your
  users to have cygwin installed, you should distribute Cygwin DLLs along
  with your binaries.

  A simple way of doing this is to create a folder with the name of your
  project, such as foobar, copy all binaries into it, and all needed Cygwin
  DLLs, which should be found under cygwin64/bin (if you installed it under
  C:\cygwin64, it would be under C:\cygwin64\bin)

  So, for instance, if you built bar.exe as in this example

  $ cd /cygdrive/e
  $ mkdir foo
  $ cp bin/bar foo
  $ cp /cygdrive/c/cygwin64/bin/cygwin1.dll foo

  Now you can compress the folder foo with zip and distribute foo.zip
  to your users. They have just to decompress the file wherever they
  feel like. Eg, on Cygwin console:

  $ zip -r foo.zip foo

  This shall create foo.zip

  Alternatively, you can use a software such as GUI tool 7-zip to
  create a self-extracting file in the form of an executable installer.

  Run the installer creation software. When prompted, select foo directory.
  Make sure you check the option to create a compressed SFX file. Stick to
  the defaults and you may create a foo-installer.exe. The user will have
  the option to run the installer and choose the install location. That's all




  