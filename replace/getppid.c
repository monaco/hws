/* getpid.c - Implementation of POSIX getppid() in Windows API (win32)

   Copyright (c) 2018, Monaco F. J.  <monaco@icmc.usp.br>

   This file is part of Hello2Worlds

   Hello2Worlds is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Windows API has a function to querying the current process id but not to 
   querying the parent process id. In order to make a POSIX-based code which
   calls getppid() portable to Windows we may implement a replacement using
   what is made available by Windows API.*/


#include    <windows.h>
#include    <tlhelp32.h>

#include <config.h>

/* 
   This is for cross compiling POSIX code on Windows using MinGW32
   If we are compiling for Cygwin, many POSIX functions are aready mau be
   aread made available by cygwin1.dll.
*/

#if HOST_OS_MINGW == 1

/* Tries to mimic POSIX int getppid(). */

int getppid()
{
  HANDLE hSnapshot = INVALID_HANDLE_VALUE;
  PROCESSENTRY32 pe32;
  int ppid = -1, pid = -1;
  
  /* Win32 API has an equivalent of POSIX getpid().
     
     The Windows API, however, does not make clear how the function behaves
     in the case where the system call fails. Our implementation just hopes
       for the best. */
  
  pid = GetCurrentProcessId(); 
  
  /* This gets a reference to a table of existing processes. */ 
  
  hSnapshot = CreateToolhelp32Snapshot( TH32CS_SNAPPROCESS, 0 );
  
  /* The idea here is to iterate through the process list until we find the 
     current process id. Then we access the information about it's parent 
     process. */
  
  if( hSnapshot != INVALID_HANDLE_VALUE ) 
    {
      ZeroMemory( &pe32, sizeof( pe32 ) );
      pe32.dwSize = sizeof( pe32 );
      if( Process32First( hSnapshot, &pe32 ) )  
	{
	  do{
	    if( pe32.th32ProcessID == pid ){    /* This is the current process.*/
	      ppid = pe32.th32ParentProcessID;	/* Get it's parent. */
	      break;
	    }
	  }while( Process32Next( hSnapshot, &pe32 ) );
	  
	}
    }
  
  if( hSnapshot != INVALID_HANDLE_VALUE )            /* Release the process table. */
    CloseHandle( hSnapshot );
  
  return ppid;
}

#endif	/* BUILD_HOST_MINGW == 1 */
