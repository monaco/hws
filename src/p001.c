/* p001.c - A trivial hello-world program.

   Copyright (c) 2018, Monaco F. J.  <monaco@usp.br>

   This file is part of Hello2Worlds

   Hello2Worlds is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/* This is a trivial Hello World example of a fully source-code portable
   program which should build without issues in any platform supporting a 
   ISO-C standard library and a compatible C runtime environment (*). 

   (*) Notes
   
   1. Both in GNU/Linux and MS Windows this program can run relying solely on
      the OS native runtime. This is what will happen if you (cross) compile it
      with MinGW; if you compile it will Cygwin, it will use Cygwin DLL,
      instead.

   2. It is even possible to compile this program with Microsoft Visual Studio
      (see the note at the bottom of this file), without any aid of autotools,
      MinGW or Cygwin.
      
*/


#include <stdio.h>		/* This from the ISO-C Standard library. */
#include <utils.h>		/* This is from the project srouce code. */

int main ()
{
  
  build_info ();      		/* Show info on build and host platforms. */

  printf ("Hello two Worlds\n");

  return 0;
}

/* 
   Note for compiling with Microsoft Visual Studio:

   a) create a new empty project;
   b) add p001.c, utils.c and utils.h to it.

   If you have some issue with printf, this is because Windows C-runtime 
   developers have decided to deprecate the standard ASNI/ISO printf in
   favor of a non-standard fprint_s in Windows API, on vindication of
   safety concerns. This mostly likely happens if you select
   a new C console application rather than an empty project when you
   create the new project with Visual Studio IDE.

   There are three simple workarounds you may resort to in order to 
   bypass this issue:

   * conditionally replace printf with printf_s (e.g. use utils.h macros)
   * pass _CRT_SECURE_NO_WARNINGS for MS Visual C++ preprocessor
   * use MSVC a pragma preprocessor directive before anywing else
     in this file to bypass MSVC complaints:

     #if defined(_MSC_VER)
     #pragma warning( disable : 4996 )
     #endif



 */
