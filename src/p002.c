/* p004.c - An example using file system path.

   Copyright (c) 2018, Monaco F. J.  <monaco@icmc.usp.br>

   This file is part of Hello2Worlds

   Hello2Worlds is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/* This examples illustrate the use of config.h to handle differences in
   both GNU/Linux and Windows APIs.  Particularly, the case of mkdir, which
   does not support POSIX ACL (Access Control List, aka 'rwx triple').


   The code also illustrates how file paths are handled by MinGW and Cygwin.
   POSIX path separator / (slash) is handled transparently --- even by 
   MS Visual C++. You may thus safely use POSIX style to specify absolute
   and relative paths.

   Notice that if you want to specify a path for a Windows system only,
   you may use Windows style separator \ (back slash). Just remember that
   back slash is understood as a escape character by the C compiler,
   such that you have to write foo\\bar to mean foo/bar.

   If a path, however, starts with a drive letter, it's considered a Windows 
   path and automatically converted to the POSIX style. In other words
   you may write "c:\foo\bar" and it will be passed unchanged.

   For more information on path handling, 
   see http://www.mingw.org/wiki/Posix_path_conversion

   Note: this code was not meant to be compiled directly on MSVC.

 */



#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <string.h>

#include <config.h>
#include <utils.h>

#define DIRNAME "foo"
#define FILENAME "test.txt"

int main ()
{
  FILE *fp;
  
  build_info();

  /* On GNU/Linux and Cygwin, POSIX mkdir() needs two arguments.
     On Windows, MinGW takes only one.*/
  
  if (access (DIRNAME, F_OK))
#if (HOST_OS_LINUX_GNU == 1) ||(HOST_OS_CYGWIN == 1)
    mkdir ("foo", 0700);
#elif HOST_OS_MINGW == 1
    mkdir ("foo");
#endif
  
  fp = fopen (DIRNAME "/" FILENAME, "w+");
  if (!fp)
    {
      printf ("%s: %d: %s\n", __FILE__ , __LINE__, strerror(errno));
      exit(1);
    }
  
  fprintf (fp, "Hello Worlds\n");

  fclose (fp);
  
  return 0;
}



