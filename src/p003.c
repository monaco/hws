/* p005.c - An example using file system path.

   Copyright (c) 2018, Monaco F. J.  <monaco@icmc.usp.br>

   This file is part of Hello2Worlds

   Hello2Worlds is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/* 
   This is an example of signal handling. 

   POSIX signal() is deprecated in favor of sigaction and so we prefer it if
   available (see configure.ac). Otherwise we resort to signal().

   It so happens that sigaction is not available under either MinGW or Cygwin.

   Notice that not all POSIX signals are available as well. SIGINT is issued 
   through Ctr-C in both GNU/Linux and Windows terminals. On the other hand,
   Ctr-Z, which on GNU/Linux issues a SIGTSTP (suspend), has a different 
   meaning on Windows (namely, it's equivalent to GNU/Linux Ctr-D).

   We see it's not always easy to mimic the same behavior despite of the
   compatibility layers provided by the different runtimes. It's therefore
   advisable to think in advance when portability is concern; explicit 
   designing and coding for portability is recommended.

 */

#include <stdio.h>
#include <unistd.h>
#include <signal.h>

#include <config.h>
#include <utils.h>

void foo (int signum)
{
  #if HAVE_DECL_SIGACTION == 1
  printf ("Foo got: %d\n", signum);
  #else
  printf ("Bar got: %d\n", signum);
  signal (SIGINT, foo);
  #endif
}


int main()
{
  #if HAVE_DECL_SIGACTION == 1
  struct sigaction act;
  #endif
  
  build_info ();

  
  #if HAVE_DECL_SIGACTION == 1	  /* GNU/Linux */

  sigaction (SIGINT, NULL, &act);
  act.sa_handler = foo;
  sigaction (SIGINT, &act, NULL);

  #else	                          /* MinGW and Cygwin. */

  signal (SIGINT, foo);
  
  #endif

  
  while (1);
  
  return 0;
}
