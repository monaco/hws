/* p002.c - A limited porting example.

   Copyright (c) 2018, Monaco F. J.  <monaco@usp.br>

   This file is part of Hello2Worlds

   Hello2Worlds is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/* This example makes use of the POSIX functions which is not  part of the 
   ISO-C Standard library.  Particularly getppid(), is not directly available
   neither by Windows C runtime nor by MinGW-w64.

   In order to make the code compilalbe across both GNU/Linux and Windows
   we use Autoconf test AC_CHECK_DECLS[(getppid)], which adds a test macro to 
   the file config.h. We use the result of this check to decide how to
   handle the POSIX function call. 

   When we compile this software GNU/Linux, getppid() is available and will
   be called.

   When we use MinGW to compile this software on Windows, or to cross-compile 
   it on GNU/Linux, getppid() is not available and therefore will be bypassed.
   It may be considered a partial-feature porting.

   When we compile this example with Cygwin on Windows, Cygwin DLL provides
   and implementation of getppid(), so it is used. This may be considered
   a full-featured porting.

*/


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

#include <config.h>
#include <utils.h>

int main ()
{

  build_info();

  #if HAVE_DECL_GETPPID
  printf ("My parent pid = %d\n", getppid());
  #else
  printf ("Process's parent pid is unavailable\n");
  #endif

  
  
  printf ("Done\n");
  return 0;
}
