/* utils.c - A few auxiliary functions.

   Copyright (c) 2018, Monaco F. J.  <monaco@icmc.usp.br>

   This file is part of Hello2Worlds

   Hello2Worlds is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
typedef enum { x86_64, i686, unknown_cpu } cpu_t;
typedef enum { linux_gnu, mingw, cygwin, windows, unknown_os } os_t;

const char *cpu_name[] = { "x86 64-bit", "x86 32-bit", "unknown" };
const char *os_name[] = { "GNU/Linux", "Windows (via MinGW)", "Windows (via Cygwin)", "Windows (natively)", "unknown" };

/* When we use Autotools, we can use information on config.h to determine 
   host and build platforms. This conditional macro is a provision for making
   the examples compilable also directly by Microsft Visual C++ without
   autotools. */

/* #if (__unix__ == 1 ) || (_gnu_linux == 1) || (__MINGW32__ == 1) || (__GYGWIN__ == 1) */

#if BUILD_SYSTEM == BUILD_SYSTEM_AUTOTOOLS

#include <config.h>


void build_info (void)
{
  cpu_t build_cpu, host_cpu;
  os_t build_os, host_os;
  
#if BUILD_CPU_X86_64 == 1
  build_cpu = x86_64;
#elif BUILD_CPU_I686 == 1
  build_cpu = i686;
#else
  build_cpu = unknown_cpu;
#endif  
  
#if BUILD_OS_LINUX_GNU == 1
  build_os = linux_gnu;
#elif BUILD_OS_MINGW == 1
  build_os = mingw;
#elif BUILD_OS_CYGWIN == 1
  build_os = cygwin;
#else
  build_os = unknown_os;
#endif
  
#if HOST_CPU_X86_64 == 1
  host_cpu = x86_64;
#elif HOST_CPU_I686 == 1
  host_cpu = i686;
#else
  host_cpu = unknown_cpu;
#endif  
  
#if HOST_OS_LINUX_GNU == 1
  host_os = linux_gnu;
#elif HOST_OS_MINGW == 1
  host_os = mingw;
#elif HOST_OS_CYGWIN == 1
  host_os = cygwin;
#else
  host_os = unknown_os;
#endif


  if ((build_cpu != unknown_cpu) && (build_os != unknown_os))
    {
      if ((build_cpu == host_cpu) && (build_os == host_os))
	printf ("Compiled for %s OS on %s CPU\n",
		os_name[host_os], cpu_name[host_cpu]);
      else
	printf ("Cross-compiled with %s OS on %s CPU for %s OS on %s CPU\n",
		os_name[build_os], cpu_name[build_cpu],
		os_name[host_os], cpu_name[host_cpu]);
    }
  else
    printf ("(Cross?)-compiled with %s SO on %s CPU for %s OS on %s CPU\n",
	    os_name[build_os], cpu_name[build_cpu],
	    os_name[host_os], cpu_name[host_cpu]);
  

}

#elif	
#ifdef _MSC_BUILD

void build_info()
{
	cpu_t build_cpu, host_cpu;
	os_t build_os, host_os;

  host_os = windows;
#if _AMD64 == 100
  host_cpu = x86_64;
#elif _M_IX86 == 600
  host_cpu = i686;
#else
  host_cpu = unknown_cpu;
#endif


  printf ("Compiled for OS %s on %s CPU\n",
	  os_name[host_os], cpu_name[host_cpu]);
}
#endif

#endif
