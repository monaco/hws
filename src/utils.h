/* utils.h - A few auxiliary functions.

   Copyright (c) 2018, Monaco F. J.  <monaco@icmc.usp.br>

   This file is part of Hello2Worlds

   Hello2Worlds is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



#ifndef UTILS_H
#define UTILS_H



#define BUILD_SYSTEM_UNKNOWN   0
#define BUILD_SYSTEM_AUTOTOOLS 1
#define BUILD_SYSTEM_WINDOWS   2


/* If we are using Autotools, then the following macros are defined by the GCC 
   port used by each one of the respective platforms supported by our project: 
   GNU/Linux, MinGW and Cygwin. 

   Microsoft Visual C++, on the other hand, defines it's own pecific macros. 
   In this case se cannot use config.h, since we assume we not using autotools. 

   We use this to know whether the project is being built witn either Autotools
   or Windows native tools provided by Visual Studio (version 2017)
*/

#if (__unix__ == 1 ) || (_gnu_linux == 1) || (__MINGW32__ == 1) || (__GYGWIN__ == 1)
# define BUILD_SYSTEM  BUILD_SYSTEM_AUTOTOOLS
#elif
# ifdef _MSC_BUILD
#   define BUILD_SYSTEM  BUILD_SYSTEM_WINDOWS
# elif
#  define BUILD_SYSTEM  UNKNOWN
# endif
#endif

/* Output building information. */

void build_info();

#endif	/* UTILS_H */
